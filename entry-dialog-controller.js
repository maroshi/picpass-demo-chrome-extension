class PicturesDialogBoxController {
    constructor(cols, rows, maxSelections, selectionsPerScreen, submit, targetPasswordElement) {
        this.isDemo = true;
        // repair static links before elements duplication
        this.repairPluginResourcesUrls(_pluginUrl);
        // duplicate columns and rows, add progress-bar links
        this.buildDialogBox(cols, rows, maxSelections);
        // assign event listners to all active elements
        this.attachEventListners();
        // create private model
        this.picturesCollection = new PicturesDialogBoxModel(maxSelections, 1, selectionsPerScreen, this);
        this.passwordElement = targetPasswordElement;
        this.keywordsPerFigure = 4;
        this.submitFlag = submit;
        this.dynamicCssStyleName = 'keywords-text-dynamic';

    }
    buildDialogBox(cols, rows, maxSelections) {
        let firstFig = document.getElementById('pp-fig-0');
        let firstRow = firstFig.parentElement;
        firstRow.style.height = `${100 / rows}%`;
        let figCount = 0;
        // duplicate figures in first row, update the id
        for (let i = 1; i < cols; i++) {
            let dupFigure = firstFig.cloneNode(true);
            dupFigure.id = "pp-fig-" + i;
            let dupPic = dupFigure.children[2];
            dupPic.id = 'pp-pic-' + i;
            firstRow.appendChild(dupFigure);
        }
        // duplicate the first row, update the id in each figure
        for (let r = 1; r < rows; r++) {
            let dupRow = firstRow.cloneNode(true);
            let dupFigsArr = dupRow.getElementsByTagName("figure");
            let dupPicsArr = dupRow.getElementsByTagName("img");
            for (let c = 0; c < dupFigsArr.length; c++) {
                dupFigsArr[c].id = "pp-fig-" + (c + (r * cols));
                dupPicsArr[c].id = "pp-pic-" + (c + (r * cols));
            }
            firstRow.parentElement.appendChild(dupRow);
        }
        // duplicate progress bar links
        let forthProgressLink = document.getElementById("pp-progress-bar").children[3];
        for (let i = 4; i < maxSelections; i++) {
            let dupProgressLink = forthProgressLink.cloneNode(true);
            dupProgressLink.innerHTML = (i + 1) + "th";
            dupProgressLink.id = "pp-kwd-" + i;
            forthProgressLink.parentElement.appendChild(dupProgressLink);
        }
        // add titles for demo version
        if (this.isDemo) {
            document.getElementById('pp-title').textContent = 'Secret-keyword hint (Demo):';
        }
    }

    attachEventListners() {
        document.getElementById("pp-close-img").addEventListener("click", onClose);
        document.getElementById("pp-settings-img").addEventListener("click", onSettings);
        document.getElementById("pp-close-button").addEventListener("click", onClose);
        document.getElementById("pp-refresh-button").addEventListener("click", onRefresh);
        document.getElementById("pp-text-button").addEventListener("click", onTextToggle);
        // let progressBarLinksArr = new Array();
        let progressBarLinksArr = document.querySelectorAll("#pp-progress-bar>a");
        progressBarLinksArr.forEach((progressBarLink) => {
            progressBarLink.addEventListener("click", onProgressBarLink);
        });
        let figuresArr = document.querySelectorAll("figure");
        figuresArr.forEach((figureElm) => {
            figureElm.addEventListener("click", onFigureSelection);
            figureElm.addEventListener("mouseover", onFigureHover);
        });
    }

    repairPluginResourcesUrls(baseUrl) {
        document.getElementById("pp-close-img").setAttribute("src", baseUrl + "common/close.png");
        document.getElementById("pp-settings-img").setAttribute("src", baseUrl + "common/settings.png");
        document.querySelector("#pp-close-button>img").setAttribute("src", baseUrl + "common/Cancel-128.png");
        document.querySelector("#pp-refresh-button>img").setAttribute("src", baseUrl + "common/Button-Refresh-icon.png");
        document.querySelector("#pp-text-button>img").setAttribute("src", baseUrl + "common/text-file-2-xxl.png");
        // document.getElementById("pp-fig-0").style.backgroundImage = "url(" + baseUrl + "common/new.png)";
    }

    getModel() {
        return this.picturesCollection;
    }

    reload() { this.getModel().loadCollection(); }

    initTextBoxStyle() {
        let imagesElm = document.getElementById('pp-images');
        let firstFigElm = document.getElementById('pp-fig-0');
        let figElmHeight = firstFigElm.offsetHeight;
        let textElmLineHeight = figElmHeight / this.keywordsPerFigure - 1;
        let cssStyle = document.createElement('style');
        let passwordElmFontSize = window.getComputedStyle(this.passwordElement, null).getPropertyValue('font-size');
        cssStyle.innerHTML = `.${this.dynamicCssStyleName} {
            font-size:${parseFloat(passwordElmFontSize)}px; 
            line-height: ${textElmLineHeight}px;
            padding-left: 5px;
        }`;
        document.body.appendChild(cssStyle);
    }

    updatePicturesSelectionScreen(imgSet, imgElmArr, secretKeyword) {
        let imgSetCount = 0;
        this.updateDemoTitle(secretKeyword);
        imgSet.forEach(function (imgElm) {
            let currTextElm = imgElmArr[imgSetCount].getElementsByTagName('div')[0];
            // update keywords text element with its current keywords
            currTextElm.innerHTML = "";
            currTextElm.classList.remove("keywords-text");

            for (let k = 0; k < imgElm.keywords.length; k++) {
                let newlineStr = (k === 0) ? "" : "&nbsp,&nbsp";
                currTextElm.innerHTML = currTextElm.innerHTML + newlineStr + imgElm.keywords[k];
            }
            // for debug and maintenance use display the image id as well.
            // currTextElm.innerHTML = currTextElm.innerHTML + ` (${imgElm.id})`

            // update figure element with correct image
            // let imgFilePath = "img/" + imgElm.id + ".jpeg";
            // console.log("imgFilePath:"+imgFilePath);
            // currTextElm.parentElement.style.backgroundImage = `url(https://localhost:8443/api/pictures/download/${imgElm.id})`;
            let htmlImgElm = currTextElm.nextElementSibling.nextElementSibling;
            downloadImage(htmlImgElm, imgElm.id);
            currTextElm.parentElement.children[1].textContent = imgElm.id;
            currTextElm.parentElement.children[2].hidden = false;

            // update image counter
            imgSetCount++;
        });
    }
    updateKeywordsSelectionScreen(keywordsSet, imgElmArr, secretKeyword, secretKeywordImg) {
        let imgSetCount = 0;
        let sortedKeywordsArr = Array.from(keywordsSet).sort();
        //
        // reduce the keywords array size to fit into diplayed list
        //
        while (sortedKeywordsArr.length > (imgElmArr.length * this.keywordsPerFigure)) {
            // pick a random keyword for removal
            let discardedKeywardIdx = this.getModel().randomIntInRange(0, sortedKeywordsArr.length - 1);
            // skip if picked on secretKeyword
            if (sortedKeywordsArr[discardedKeywardIdx] === secretKeyword) {
                continue;
            }
            // remove from array
            sortedKeywordsArr.splice(discardedKeywardIdx, 1);
        }
        let sortedKeywordsArrCount = 0;
        this.updateDemoTitle(secretKeyword);
        for (let currElm = 0; currElm < imgElmArr.length; currElm++) {
            //
            // fill each text element with a collection of keywords
            //
            let currTextElm = imgElmArr[currElm].getElementsByTagName('div')[0];

            currTextElm.innerHTML = "";
            // remove background image from figure element (parent element)
            currTextElm.parentElement.style.backgroundImage = 'none';
            // assign random id to each figure element (parenet element)
            let randomId = this.getModel().randomIntInRange(100, 165);
            currTextElm.parentElement.children[1].textContent = randomId;
            for (let i = 0; i < this.keywordsPerFigure; i++) {
                //
                // fill keywords into figure>.keywords-text element
                //
                let newlineStr = (i === 0) ? "" : "<br>";
                if (sortedKeywordsArrCount >= sortedKeywordsArr.length) continue;
                let currKeyword = sortedKeywordsArr[sortedKeywordsArrCount];
                currTextElm.innerHTML = currTextElm.innerHTML + newlineStr + currKeyword;
                if (currKeyword === secretKeyword) {
                    //
                    // if this is the secret keyword, associate figure element to corret _imgArr obj
                    // assign the figure id with the same id as the _imgArr obj id
                    // 
                    currTextElm.parentElement.children[1].textContent = secretKeywordImg.id;
                }
                sortedKeywordsArrCount++;
            }
            // hide the picture-
            currTextElm.parentElement.children[2].hidden = true;
        }
    }
    updateDemoTitle(secretKeyword) {
        if (this.isDemo) {
            document.getElementById('pp-header-text').textContent = secretKeyword;
        }
    }
    imageSelection(htmlElm) {
        let imgObjId = parseInt(htmlElm.children[1].textContent);
        let imgArrObj = SecretKeyword.getImageObjById(imgObjId);
        // console.log("imgArrId="+imgArrId+", imgArrObj.pswdSgmt="+imgArrObj.pswdSgmt);
        // get the password segment (real or fake do not care)
        let pwdSegment = `☹${imgObjId}`;
        if (typeof imgArrObj !== 'undefined') {
            pwdSegment = imgArrObj.pswdSgmt;
        }

        // if first selection, reset password text box 
        if (this.getModel().selectionsCount === 1) {
            this.passwordElement.value = '';
        }

        // update the target password element value     
        let pwdEntryText = this.passwordElement.value;
        pwdEntryText = pwdEntryText + pwdSegment;
        this.passwordElement.value = pwdEntryText;
        console.debug("pwdEntryText=", pwdEntryText);

        // append pwdSegment to insertions history
        this.getModel().selectionsPswdSgmntArr[this.getModel().selectionsCount - 1] = pwdEntryText;
        // udpate selection counter
        this.getModel().selectionsCount++;
        // reset the paswdSgmt (in case it contained a real password segment)
        if (typeof imgArrObj !== 'undefined') {
            imgArrObj.pswdSgmt = imgArrObj.id;
        }

        // if last selection
        if (this.getModel().selectionsCount > this.getModel().selectionsMax) {
            // Selected the last image and close the modal dialog.
            this.getModel().selectionsPswdSgmntArr.length = 0;
            this.getModel().selectionsCount = 1;
            document.getElementById('dialog-1').close();
            if (this.submitFlag) {
                // find the login btn
                let loginBtn = document.querySelectorAll('*[type=submit]')[0]
                // if found a login btn submit it by clicking on it
                if (loginBtn) loginBtn.click();
            }
            return;
        }
        // load the next screen if no more selections on this screen
        if (!this.getModel().isPicturesList) {
            // on text screen just reload the screen
            this.getModel().loadCollection();
        } else {
            // on pictures screen reload the screen only when required
            if (((this.getModel().selectionsCount - 1) % this.getModel().selectionsPerScreen) === 0) {
                this.getModel().loadCollection();
            } else {
                // just update the secretKeyword 
                this.updateDemoTitle(this.getModel().getCurrentSecretKeyword());
            }
        }
    }
    updateProgressBar() {
        let progressLinksArr = document.querySelectorAll("#pp-progress-bar>a");
        for (let i = 0; i < progressLinksArr.length; i++) {
            let visibilityValue = (i < this.getModel().selectionsCount) ? "visible" : "hidden";
            progressLinksArr[i].style.visibility = visibilityValue;
        }
    }
    toggleListDisplay() {
        if (this.getModel().isPicturesList) {
            document.querySelectorAll('#pp-images figure').forEach((figElm) => {
                figElm.classList.add("fig-text");
                figElm.getElementsByTagName('div')[0].classList.add(this.dynamicCssStyleName);
            });
            this.getModel().isPicturesList = false;
        } else {
            document.querySelectorAll('#pp-images figure').forEach((figElm) => {
                figElm.classList.remove("fig-text");
                figElm.getElementsByTagName('div')[0].classList.remove(this.dynamicCssStyleName);
            });
            this.getModel().isPicturesList = true;
        }
        this.getModel().loadCollection();
    }
    rewindSelectionTo(selectionRequest) {
        if (this.getModel().selectionsCount == 1) return;
        if (selectionRequest == 1) {
            this.passwordElement.value = "";
        } else {
            this.passwordElement.value = this.getModel().selectionsPswdSgmntArr[selectionRequest - 2];
        }
        this.getModel().selectionsCount = selectionRequest;
        // console.debug("rewinded passwordEntry = ", this.passwordElement.value);
        this.getModel().loadCollection();
    }
}

async function downloadImage(htmlImgElm, picId) {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', `${_webServiceUrl}pictures/download/${picId}`, true);
    xhr.responseType = 'blob';
    xhr.onload = function (e) {
        htmlImgElm.src = window.URL.createObjectURL(this.response);
    };
    xhr.send();
}

function onClose(event) {
    console.log("onColse entry");
    let dialog = document.getElementById('dialog-1');
    dialog.close();
}

function onRefresh(event) {
    console.log("onRefresh entry");
    _picturesDialogBox.reload();
}

function onTextToggle(event) {
    console.log("onTextToggle entry");
    _picturesDialogBox.toggleListDisplay();
}

function onProgressBarLink(event) {
    console.log("onProgressBarLink entry");
    let selectionCountIdx = event.target.id.lastIndexOf("-");
    let selectionCount = parseInt(event.target.id.substr(selectionCountIdx + 1)) + 1;
    _picturesDialogBox.rewindSelectionTo(selectionCount);
}

function onFigureSelection(event) {
    let targetElm = event.target;
    while (targetElm.nodeName !== "FIGURE") { targetElm = targetElm.parentNode; }
    console.log("onFigureSelection: selected figure=", targetElm.id);
    _picturesDialogBox.imageSelection(targetElm);
}

function onSettings(event) {
    let dialog = document.getElementById('dialog-1');
    dialog.close();
    onPasswordRegistrationInvoked();
}

function onFigureHover(event) {
    if (event.ctrlKey == true) {
        // $("<a>").attr("href", activeLink).attr("target", "_blank")[0].click();
        // console.dir(event.target.parent.children[1]);
        let targetElm = event.target;
        if (targetElm.tagName === 'IMG') {
            let picId = targetElm.parentElement.getElementsByTagName('details')[0].textContent;
            console.log(`current picId: ${picId}`);
            chrome.storage.local.set({ "markedPicId": picId }, function () {
                // console.dir(["Saved markedPicId", picId]);
            });
       }
    }
}