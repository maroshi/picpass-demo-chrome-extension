class PicturesDialogBoxModel {
    constructor(max = 4, count = 1, selectionsPerScreen = 1, picturesDialogBoxController) {
        this.selectionsMax = max;
        this.selectionsCount = count;
        this.selectionsPerScreen = selectionsPerScreen;
        this.selectionsPswdSgmntArr = new Array(this.selectionsMax);
        this.isPicturesList = true; // false = isKeywordsList
        this.controller = picturesDialogBoxController;
    }
    randomIntInRange(low, high) {
        return Math.floor((Math.random() * (high - low + 1))) + low;
    }

    appendKeywordsAndImgToSets(imgObj, keywordsSet, imgSet) {
            for (let i = 0; i < imgObj.keywords.length; i++) {
                keywordsSet.add(imgObj.keywords[i]);
            }
            imgSet.add(imgObj);
        }
        // loads (or reloads) picModel with current list
    loadCollection() {
        // get all img element into array
        let figureElmArr = document.getElementsByTagName('figure');
        let imgSet = new Set();
        let keywordsSet = new Set();
        // insert this screen secretKeywordImg(s) into keywordsSet and imgSet
        let secretKeywordImgsArr = this.getSecretKeywordsPicArr(this.selectionsCount - 1);
        if (secretKeywordImgsArr == null) {
            return;
        }
        secretKeywordImgsArr.forEach((secretKeywordImgObj) => {
            this.appendKeywordsAndImgToSets(secretKeywordImgObj, keywordsSet, imgSet);
        });
        // populate imgSet containing _imgArr objects for each img element
        let whileLoopCount = 0;
        while (imgSet.size < figureElmArr.length) {
            // break the loop if fail finding an image for too long
            if (whileLoopCount > 500) {
                alert("Failed match pictures list on " + imgSet.size + "th image, Please retry!");
                return;
            } else {
                whileLoopCount++;
            }
            // get the next image using random
            let randomImgSelectionFromImgArr = _imgArr[this.randomIntInRange(0, _imgArr.length - 1)];
            if (imgSet.has(randomImgSelectionFromImgArr)) { // skip if got this img already
                continue;
            }
            // using Set object becuase of useful Set.has() method in intersection calculation
            let imgKeywordsSet = new Set(randomImgSelectionFromImgArr.keywords);
            //
            // intersect keywordsSet and imgKeywordsSet
            // skip this image if has intersecting keywords with imgKeywordsSet
            //
            let keywordsIntesectionArr = Array.from(keywordsSet);
            keywordsIntesectionArr = keywordsIntesectionArr.filter(function(x) {
                return imgKeywordsSet.has(x);
            });
            if (keywordsIntesectionArr.length > 0) {
                // skip if there is intersection
                continue;
            }
            // img is accepted append to the keywords set and img set
            this.appendKeywordsAndImgToSets(randomImgSelectionFromImgArr, keywordsSet, imgSet);
        }
        //
        // update picture list or text list
        //
        if (this.isPicturesList) {
            // this is a picturesList
            //
            // put the secret pictures in a random position on the screen.
            //
            let secretImgPostion = this.randomIntInRange(0, imgSet.size - 1);
            let orderedImgArr = Array.from(imgSet);
            for (let i = 0; i < secretKeywordImgsArr.length; i++) {
                // swap imgObj postion
                let tmpVar = orderedImgArr[i];
                orderedImgArr[i] = orderedImgArr[secretImgPostion];
                orderedImgArr[secretImgPostion] = tmpVar;
                // rewrite the img Set
                imgSet = new Set(orderedImgArr);
            }
            this.controller.updatePicturesSelectionScreen(imgSet,
                figureElmArr,
                this.getCurrentSecretKeyword());
        } else {
            // this is a keywordsList
            this.controller.updateKeywordsSelectionScreen(keywordsSet,
                figureElmArr,
                this.getCurrentSecretKeyword(),
                secretKeywordImgsArr[0]);
        }
        this.controller.updateProgressBar();
    }
    getCurrentSecretKeyword() {
        let kwdArrIdx = (this.selectionsCount - 1) % _secretKeywordsArr.length;
        return _secretKeywordsArr[kwdArrIdx].keyword;
    }
    getSecretKeywordsPicArr(secretKeywordIdx) {
        // calc secretKeywords in this screen
        let selectionsInCurrScreen = this.selectionsPerScreen;
        if (secretKeywordIdx + this.selectionsPerScreen > this.selectionsMax) {
            selectionsInCurrScreen = this.selectionsMax - secretKeywordIdx;
        }

        let returnImgObjArr = []; // function's return array
        let secretKeywordsArr = [];
        for (let i = 0; i < selectionsInCurrScreen; i++) {
            let currSecretKeyword = _secretKeywordsArr[(secretKeywordIdx + i) % _secretKeywordsArr.length];
            secretKeywordsArr.push(currSecretKeyword);
        }
        let picIdArr = _kwdCtrl.picIdArrForSecretKeywords(secretKeywordsArr);
        if (picIdArr.length == 0) {
            this.handleErrorMessage(secretKeywordsArr, secretKeywordIdx);
            return returnImgObjArr; // empty array
        }
        for (let count = 0; count < secretKeywordsArr.length; count++) {
            let currSecretKeyword = secretKeywordsArr[count];
            let imgObj = SecretKeyword.getImageObjById(picIdArr[count]);
            // update the imgId pswdSgmt
            imgObj.pswdSgmt = '';
            if (secretKeywordIdx + count < _secretKeywordsArr.length) {
                imgObj.pswdSgmt = currSecretKeyword.pswdSgmt;
            }
            returnImgObjArr.push(imgObj);
        }
        return returnImgObjArr;
    }
    handleErrorMessage(secretKeywordsArr, curr) {

        let errMessage = `Failed load picutures for secret keywords, due to  pictures' keywords overlap:`;
        let firstKeyword = true;
        for (let count = 0; count < secretKeywordsArr.length; count++) {
            let collisionKeyword = _secretKeywordsArr[curr + count].keyword;
            let prefix = (firstKeyword) ? "" : ","
            firstKeyword = false;
            errMessage += `${prefix} '${collisionKeyword}'`
        }
        errMessage += "."
        console.error(errMessage);
        alert(errMessage +
            `

Corrective actions (any of the following):
1. Change keywords.
2. Reorder keywords to different screens.
3. Use 1 keyword per screen.
4. Change pictures.
`);
    }
}