
// call https://localhost:8443/api/pictures?projection=embedKeywords
var _webServiceUrl = `https://localhost:8443/api/`;
const getPicsWithKeywords = async () => {
    const response = await fetch(`${_webServiceUrl}pictures?projection=embedKeywords`);
    let picsWithKeywordsObjsArr = await response.json(); //extract JSON from the http response
    picsWithKeywordsObjsArr = picsWithKeywordsObjsArr._embedded.pictures;
    // console.dir(picsWithKeywordsObjsArr);
    picsWithKeywordsObjsArr.forEach(function (currPicObj) {
        let keywordsTextArr = [];
        currPicObj.keywords.forEach(function (currKeywordObj) {
            keywordsTextArr.push(currKeywordObj.name);
        });
        currPicObj.keywords = keywordsTextArr;
        currPicObj.pswdSgmt = `☹${currPicObj.id}`;
    });
    return picsWithKeywordsObjsArr;
}
const getAllKeywords = async () => {
    const response = await fetch(`${_webServiceUrl}keywordPicsCounts/search/all`);
    let getAllKeywordsArr = await response.json(); //extract JSON from the http response
    getAllKeywordsArr = getAllKeywordsArr._embedded.keywordPicsCounts;
    // console.dir(getAllKeywordsArr);
    return getAllKeywordsArr;
}


class SecretKeyword {
    constructor(keyword, pswdSgmt, picsIdArr) {
        this.keyword = keyword;
        this.pswdSgmt = pswdSgmt;
        this.picsIdArr = picsIdArr;
        this.currentImgId = undefined;
    }
    incrementImgId() {
        this.currentImgId++;
        // if exhausted the secret password images, restart from the first 
        if (this.currentImgId === this.picsIdArr.length) {
            this.currentImgId = 0;
        }
    }
    getImageObj() {
        let secretImgId = this.picsIdArr[this.currentImgId];
        return getImageObjById(secretImgId);
    }
    static getImageObjById(secretImgId) {
        let imgObj = _imgArr.find((img) => {
            return img.id === secretImgId;
        });
        return imgObj;
    }
    diffPicsIdArr(otherSecretKeywordObj) {
        let aPicsIdArr = [...this.picsIdArr];
        let diffPicsIdArr = [];
        for (let currPicId of aPicsIdArr) {
            diffPicsIdArr = aPicsIdArr.filter((currPicId) => {
                let foundInOtherPicId = otherSecretKeywordObj.picsIdArr.find((otherPicId) => {
                    return otherPicId === currPicId;
                });
                return (foundInOtherPicId === undefined) ? true : false;
            });
        }
        this.picsIdArr = diffPicsIdArr;
        return diffPicsIdArr;
    }
}

class KeywordsController {
    constructor() {
        if (!KeywordsController.instance) {
            this.keywordsMap = new Map();
            this.keywordFreequency = 0;
            this.freequenetKeywordsMap = new Map();
            this.addAllKeywords();
            this.setKeywordsFreequencyThreshold(4);
            KeywordsController.instance = this;
        }
        return KeywordsController.instance;
    }
    createSecretKeywordObj(keyword, pswdSgmnt) {
        if (!this.freequenetKeywordsMap.has(keyword)) {
            return undefined;
        }
        let newSecretKeywordObj = new SecretKeyword(keyword,
            pswdSgmnt,
            this.freequenetKeywordsMap.get(keyword));
        return newSecretKeywordObj;
    }
    addKeyword(keyword) {
        let picsIdSet = new Set();
        _imgArr.forEach((imgObj) => {
            let hasKeyword = imgObj.keywords.find((currKeyword) => {
                return currKeyword === keyword;
            });
            if (hasKeyword) { picsIdSet.add(imgObj.id); }
        });
        let picsIdArr = Array.from(picsIdSet);
        this.keywordsMap.set(keyword, picsIdArr);
    }
    addAllKeywords() {
        _imgArr.forEach((imgObj) => {
            imgObj.keywords.forEach((currKeyword) => {
                if (this.keywordsMap.has(currKeyword) === false) {
                    this.addKeyword(currKeyword);
                }
            });
        });
    }
    getKeywordsMapWithFewPics(minPicsCount) {
        let frequestKeywordsMap = new Map();
        let keywordsMap = this.keywordsMap;
        keywordsMap.forEach((picsIdArr, keyword, keywordsMap) => {
            if (picsIdArr.length >= minPicsCount) {
                frequestKeywordsMap.set(keyword, picsIdArr);
            }
        });
        return frequestKeywordsMap;
    }
    getKeywordsMap() {
        return this.keywordsMap;
    }
    getFreequentKeywordsMap() {
        return this.freequenetKeywordsMap;
    }
    getFreequentKeywordsMapWith(minPicsCount) {
        return getKeywordsMapWithFewPics(minPicsCount);
    }
    setKeywordsFreequencyThreshold(minPicsCount) {
        this.keywordFreequency = minPicsCount;
        this.freequenetKeywordsMap = this.getKeywordsMapWithFewPics(this.keywordFreequency);
    }
    shuffleArray(arr) {
        if (arr.length == 1)
            return;
        for (var i = 0; i < arr.length; i++) {
            var r = Math.floor(Math.random() * (i + 1));
            var t = arr[i];
            arr[i] = arr[r];
            arr[r] = t;
        }
    }
    picIdArrForSecretKeywords(secretKeywordsObjArr) {
        this.shuffleArray(secretKeywordsObjArr); // shuffleArray
        // condense
        for (let i = 1; i < secretKeywordsObjArr.length; i++) {
            for (let h = 0; h < i; h++) {
                secretKeywordsObjArr[h].diffPicsIdArr(secretKeywordsObjArr[i]);
            }
        }
        // create picId permutation arrayOfArray
        let picIdArrOfArr = [];
        for (let secretKeywordsObj of secretKeywordsObjArr) {
            this.shuffleArray(secretKeywordsObj.picsIdArr);
            picIdArrOfArr.push(secretKeywordsObj.picsIdArr);
        }
        if (secretKeywordsObjArr.length == 1) {
            return [secretKeywordsObjArr[0].picsIdArr[0]];
        }
        let picIdPrmtnsArrOfArr = [];
        this.arraysPermutations(picIdArrOfArr, 0, [], picIdPrmtnsArrOfArr);

        // return the first permuation array with no keywords collisions/overlap
        prmtnsLoop:
        for (let picIdPrmtnsArr of picIdPrmtnsArrOfArr) {

            let keywordsSet = new Set();
            // scan all PicIds in picIdPrmtnsArr
            for (let picId of picIdPrmtnsArr) {
                // scan all kewyords for PicId
                let keywordsArr = SecretKeyword.getImageObjById(picId).keywords;
                for (let keyword of keywordsArr) {
                    if (!keywordsSet.has(keyword)) {
                        keywordsSet.add(keyword);
                    } else {
                        continue prmtnsLoop;
                    }
                }
            }
            // if reached here success, found a good permuatation
            return picIdPrmtnsArr;
        }
        return [];
    }

    /*
    // arraysPermutations example usage:
    let ar1 = ['a', 'b', 'c'];
    let ar2 = ['1', '2'];
    let ar3 = ['①', '②'];
    let ar4 = ['➊', '➋', '➌', '➍'];

    let arr = [ar2, ar3, ar4];
    let rslt = []
    arraysPermutations(arr, 0, [], rslt);
    console.log(rslt);

    [ 
    [ '1', '①', '➊' ],
    [ '1', '①', '➋' ],
    [ '1', '①', '➌' ],
    [ '1', '①', '➍' ],
    [ '1', '②', '➊' ],
    [ '1', '②', '➋' ],
    [ '1', '②', '➌' ],
    [ '1', '②', '➍' ],
    [ '2', '①', '➊' ],
    [ '2', '①', '➋' ],
    [ '2', '①', '➌' ],
    [ '2', '①', '➍' ],
    [ '2', '②', '➊' ],
    [ '2', '②', '➋' ],
    [ '2', '②', '➌' ],
    [ '2', '②', '➍' ] ]

    */
    arraysPermutations(inArrOfArr, level, arg, outArrOfArr) {
        for (let elm of inArrOfArr[level]) {
            if (level < inArrOfArr.length - 1) {
                this.arraysPermutations(inArrOfArr, level + 1, [...arg, elm], outArrOfArr);
            } else if (level == inArrOfArr.length - 1) {
                outArrOfArr.push([...arg, elm]);
            }
        }
    }
    createSecretKewrodsArr(kewordsStrArr, passwordStr) {
        let kwdsCount = kewordsStrArr.length;
        let pswdLen = passwordStr.length;
        let avgSgmnLength = pswdLen / kwdsCount;

        // prepare the length of each pswdSgmnt
        let pswdSgmntLenArr = new Array(kwdsCount);
        // each pswdSgmnts length is floor(avgSgmnLength) or ciel(avgSgmnLength)
        let minPswdSgmntLen = Math.floor(avgSgmnLength);
        pswdSgmntLenArr.fill(minPswdSgmntLen);
        let rmndrPwdLtrs = pswdLen % kwdsCount;
        for (let i = 0; i < rmndrPwdLtrs; i++) {
            pswdSgmntLenArr[i] = minPswdSgmntLen + 1;
        }
        // add extra pswdSgmnts length variance
        if (minPswdSgmntLen > 0 && kwdsCount > 3) {
            pswdSgmntLenArr[0]++;
            pswdSgmntLenArr[1]--;
        }

        // create SecretKeyword objs and populate their pswdSgmnts
        let secretKeywordObjArr = [];
        this.shuffleArray(pswdSgmntLenArr);
        let pswdIdx = 0;
        for (let i = 0; i < kwdsCount; i++) {
            let currPwsSgmnt = passwordStr.substr(pswdIdx, pswdSgmntLenArr[i]);
            pswdIdx += pswdSgmntLenArr[i];
            let currScrtKwdObj = this.createSecretKeywordObj(kewordsStrArr[i], currPwsSgmnt);
            secretKeywordObjArr.push(currScrtKwdObj);
        }
        return secretKeywordObjArr;
    }
}

var _allKeywordsArr 
getAllKeywords().then ((resp) => {_allKeywordsArr = resp});
var _imgArr = [];
var _kwdCtrl = {};
var _secretKeywordsArr = [];
getPicsWithKeywords().then(function (resp) {
    _imgArr = resp;
    _kwdCtrl = new KeywordsController();
    Object.freeze(_kwdCtrl);
    /*
    _secretKeywordsArr = _kwdCtrl.createSecretKewrodsArr([
        'horse',
        'drink',
        'dog',
        'swim'
    ], "picpass01");
    */
    _secretKeywordsArr = _kwdCtrl.createSecretKewrodsArr([
        'blue',
        'woman',
        'fly',
        'dog'
    ], "picpass01");
});

/*
var xhr = new XMLHttpRequest();
xhr.open('GET', 'https://localhost:8443/api/pictures/download/64', true);
xhr.responseType = 'blob';
xhr.onload = function(e) {
  var img = document.createElement('img');
  img.src = window.URL.createObjectURL(this.response);
  document.body.appendChild(img);
};
xhr.send();
*/

