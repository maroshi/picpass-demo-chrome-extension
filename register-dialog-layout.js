var _dialogPwdRegisterHtmlLayout = `
<main>
    <section id="pr-header-section">
        <div id="pr-title"><h1>Register pasword with keywords</h1></div>
        <p>
        <div id="pr-header-text"><h3>Asign keywords and questions, to password.</h3></div>
     </section>
    <section id="pr-form-section">
        <form id="pr-form">
           <div>
               <label for="password-field">Password or pin:</label>

               <input id="pr-password-field" name="password-field" type="email" placeholder="Pasword" required autofocus>
               <p>
            </div>
            <fieldset>
                <legend>Secret keywords and questions</legend>
                <div><p>
                    <label for="secret-keyword-1">Secret keyword 1:</label>
                    <select id="pr-select-keyword-1" name="secret-keyword-1" ></select>
                </div>
               <div><p>
                    <label for="secret-keyword-2">Secret keyword 2:</label>
                    <select id="pr-select-keyword-2" name="secret-keyword-2" ></select>
                </div>
               <div><p>
                    <label for="secret-keyword-3">Secret keyword 3:</label>
                    <select id="pr-select-keyword-3" name="secret-keyword-3" ></select>
                </div>
                <div><p>
                    <label for="secret-keyword-4">Secret keyword 4:</label>
                    <select id="pr-select-keyword-4" name="secret-keyword-4" ></select>
                </div>
                <div><p>
                    <label for="secret-keyword-5">Context/application specific question:</label>
                    <select id="pr-select-keyword-5" name="secret-keyword-5" >
                        <option value="Phone: Identify person called you yesterday for 5 min converstaion.">Phone: Identify person called you yesterday for 5 min converstaion.</option>
                        <option value="Phone: Identify the city you spent last evening.">Phone: Identify the city you spent last evening.</option>
                        <option value="Payment and credit card: Identify the item last purchased">Payment and credit card: Identify the item last purchased.</option>
                        <option value="Payment and credit card: Identify the city where item last purchased.">Payment and credit card: Identify the city where item last purchased.</option>
                        <option value="Email and messaging: Identify person you replied to 3 times yesterday.">Email and messaging: Identify person you replied to 3 times yesterday.</option>
                        <option value="Email and messaging: Identify contact you added yesterday.">Email and messaging: Identify contact you added yesterday.</option>
                        <option value="Personal: Identify place from your childhood neighborhood.">Personal: Identify place from your childhood neighborhood.</option>
                        <option value="Personal: Identify family member.">Personal: Identify family member.</option>
                        <option value="Personal: Identify item in your home street.">Personal: Identify item in your home street.</option>
                        </select>
            </div>
         </fieldset>
        </form>
    </section>
     <section id="pr-footer-section">
        <div>
            <p>
            <button id="pr-ok-button" type="button" >OK</button>
            <button id="pr-shuffle-button" type="button">Shuffle</button>
            <button id="pr-cancel-button" type="button">Cancel</button>
        </div>
    </section>
 </main>
`