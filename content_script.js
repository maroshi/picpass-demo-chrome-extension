// The background page is asking us to find password input(s) on the page.
if (window == top) {
    chrome.extension.onMessage.addListener(function (req, sender, sendResponse) {
        if (req.type == "hasPasswordField") {
            sendResponse(findPasswordField(req.param));
        } else {
            console.error("received request for unknown message", sender);
        }
    });
}
var _pluginUrl;
var _picturesDialogBox;


var findPasswordField = function (passwordEntryParams) {
    // console.log("called findPasswordField ");
    let passwordElementsArr = document.querySelectorAll("input[type=password]");
    let found = false;
    passwordElementsArr.forEach((pwdElm) => {
        console.log(`pwdElm.hidden=${pwdElm.getAttribute('hidden')}`);
        console.log(`pwdElm.aria-hidden=${pwdElm.getAttribute('aria-hidden')}`);
        if (pwdElm.getAttribute('hidden') == null || pwdElm.getAttribute('hidden') == false) {
            found = true;
        }
    });

    // console.log(`findPasswordField returned ${found}`);
    if (!found) return found;
    /*
    // Attach double click event listener to passwordElements
    */
    passwordElementsArr.forEach((passwordElement) => {
        // console.log(`event registration:`);
        passwordElement.addEventListener("dblclick", function () {
            // console.log(`double click event registration:`);
            onPasswordFieldInvoked(passwordElement, passwordEntryParams);
        });
        passwordElement.addEventListener("click", function () {
            // console.log(`click event registration:`);
            if (window.event.ctrlKey) {
                onPasswordRegistrationInvoked();
            }
        });
    });
    return found;
};

var _registerPasswordDialog;
var onPasswordRegistrationInvoked = function () {
    console.log(`<ctrl>-click  registered`);
    _registerPasswordDialog = new RegisterPasswordDialogController();
    _registerPasswordDialog.show();
}

var onPasswordFieldInvoked = function (passwordElement, passwordEntryParams) {
    initDialog(passwordEntryParams, passwordElement);
    console.info(`${new Date()} Double clicked on password field.`);
    console.log("passwordEntryParams=", passwordEntryParams);
    passwordElement.value = '';
    console.log("passwordElement=", passwordElement);
    dialogShowModal();
};

function initDialog(passwordEntryParams, passwordElement) {
    if (document.getElementById('dialog-1') == null) {
        let dialogContainterElm = document.createElement('dialog');
        dialogContainterElm.innerHTML = _dialogHtmlLayout;
        dialogContainterElm.id = 'dialog-1';
        document.body.appendChild(dialogContainterElm);
        _pluginUrl = passwordEntryParams.pluginUrl;
        _picturesDialogBox = new PicturesDialogBoxController(passwordEntryParams.columns,
            passwordEntryParams.rows,
            passwordEntryParams.maxSelections,
            passwordEntryParams.selectionsPerScreen,
            passwordEntryParams.submitOnCompletedEntry,
            passwordElement);
    }
}

function dialogShowModal() {
    let dialog = document.getElementById('dialog-1');
    dialog.removeAttribute('open');
    dialog.showModal();
    document.getElementById('pp-close-img').contentEditable = true;
    document.getElementById('pp-close-img').focus();
    // init text box parameters with dynamic values after dialog is created
    _picturesDialogBox.initTextBoxStyle();
    // display the dialog box
    onRefresh();
}