function setOptionsBy(params) {
    document.getElementById('rows').value = params.rows;
    document.getElementById('columns').value = params.columns;
    document.getElementById('selectionsPerScreen').value = params.selectionsPerScreen;
    document.getElementById('maxSelections').value = params.maxSelections;
    document.getElementById('submitOnCompletedEntry').checked = params.submitOnCompletedEntry;
}

function onCancelButton() {
    console.log("onCancelButton");
    window.close();
}

function onDefaultButton() {
    console.log("onDefaultButton");
    hasPasswordMessage.param = _defaultScreenParams;
    setOptionsBy(hasPasswordMessage.param);
}

function onSaveButton() {
    console.log("onSaveButton");
    hasPasswordMessage.param.rows = document.getElementById('rows').value;
    hasPasswordMessage.param.columns = document.getElementById('columns').value;
    hasPasswordMessage.param.selectionsPerScreen = document.getElementById('selectionsPerScreen').value;
    hasPasswordMessage.param.maxSelections = document.getElementById('maxSelections').value;
    hasPasswordMessage.param.submitOnCompletedEntry = document.getElementById('submitOnCompletedEntry').checked;

    chrome.storage.local.set({ 'param': hasPasswordMessage.param }, function() {
        // Notify that we saved.
        console.log("screenParams saved!");
        window.close();
        chrome.tabs.reload(selectedTabId, null);
    });
}


document.getElementById('cancel-button').addEventListener('click', onCancelButton);
document.getElementById('default-button').addEventListener('click', onDefaultButton);
document.getElementById('save-button').addEventListener('click', onSaveButton);
chrome.storage.local.get('param', function(loadedObj) {
    hasPasswordMessage.param = _defaultScreenParams;
    if (loadedObj.param !== undefined) { hasPasswordMessage.param = loadedObj.param; }
    setOptionsBy(hasPasswordMessage.param);
});