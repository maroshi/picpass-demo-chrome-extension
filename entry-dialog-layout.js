var _dialogHtmlLayout = `
<main>
    <section id="pp-header">
        <div id="pp-title">Dialog title:</div>
        <div id="pp-header-text">header text</div>
        <img id="pp-settings-img" src="common/settings.png" alt="settings"> 
        <img id="pp-close-img" src="common/close.png" alt="close"> 
    </section>
    <section id="pp-images">
        <div class="row">
            <figure id="pp-fig-0">
                <div class="keywords-text">keyword-1
                    <br>keyword-2
                    <br>keyword-3
                    <br>keyword-4</div>
                <details class="hidden-img-obj-id"></details>
                <img id="pp-pic-0" height="100%" width="100%" >
            </figure>
        </div>
    </section>
    <section id="pp-footer">
        <div id="pp-progress-bar"> <a id="pp-kwd-0" href="#">1st</a> <a id="pp-kwd-1" href="#">2nd</a> <a id="pp-kwd-2" href="#">3rd</a><a id="pp-kwd-3" href="#">4th</a></div>
        <div id="pp-tool-bar">
            <button type="button" id="pp-close-button"><img src="common/Cancel-128.png" alt="close" height="36px"></button>
            <button type="button" id="pp-refresh-button"><img src="common/Button-Refresh-icon.png" alt="refresh" height="36px"></button>
            <button type="button" id="pp-text-button"><img src="common/text-file-2-xxl.png" alt="text" height="36px"></button>
        </div>
    </section>
</main>
`