class RegisterPasswordDialogController {
    constructor() {
        this.dialog = document.getElementById('dialog-2');
        if (document.getElementById('dialog-2') == null) {
            let dialogContainterElm = document.createElement('dialog');
            dialogContainterElm.innerHTML = _dialogPwdRegisterHtmlLayout;
            dialogContainterElm.id = 'dialog-2';
            document.body.appendChild(dialogContainterElm);
    
            dialogContainterElm.removeAttribute('open');
            this.dialog = dialogContainterElm;
    
            let keywordsCountThreshold = 7;
            this.keywordSelectElm_1 = document.getElementById('pr-select-keyword-1');
            this.keywordSelectElm_2 = document.getElementById('pr-select-keyword-2');
            this.keywordSelectElm_3 = document.getElementById('pr-select-keyword-3');
            this.keywordSelectElm_4 = document.getElementById('pr-select-keyword-4');
            _allKeywordsArr.some((aKeyword) => {
                if (aKeyword.picsCount >= keywordsCountThreshold) {
                    let optionElm = document.createElement("option");
                    optionElm.text = aKeyword.name;
                    switch(aKeyword.category) {
                        case 'noun':
                        this.keywordSelectElm_2.add(optionElm);
                        let optionElm2 = document.createElement("option");
                        optionElm2.text = aKeyword.name;
                        this.keywordSelectElm_4.add(optionElm2);
                        break;
                        case 'verb':
                        this.keywordSelectElm_3.add(optionElm);
                        break;
                        case 'descr':
                        this.keywordSelectElm_1.add(optionElm);
                        break;
                    }
                    return false;
                } else if (aKeyword.picsCount < keywordsCountThreshold) {return true;}
            });
            // attach the current password
            let pwd = "";
            _secretKeywordsArr.forEach((pwdSeg) => {
                pwd = pwd + pwdSeg.pswdSgmt;
            });
            document.getElementById('pr-password-field').value =pwd;

            //attach event handlers
            document.getElementById("pr-ok-button").addEventListener("click", onRegisterDialog_ok);
            document.getElementById("pr-shuffle-button").addEventListener("click", onRegisterDialog_shuffle);
            document.getElementById("pr-cancel-button").addEventListener("click", onRegisterDialog_close);    
        }        
    }
    show() {this.dialog.showModal();}

    randomIntInRange(low, high) {
        return Math.floor((Math.random() * (high - low + 1))) + low;
    }

    selectRandomValue(selectElm) {        
        let selectionsCount = selectElm.length;
        let randomSelection = this.randomIntInRange(0, selectionsCount - 1);
        selectElm.selectedIndex = randomSelection;      
    }
}

function onRegisterDialog_close() {
    console.log("onRegisterDialog_close entry");
    let dialog = document.getElementById('dialog-2');
    dialog.close();
}
function onRegisterDialog_shuffle() {
    console.log("onRegisterDialog_shuffle entry");
    let keywordSelectElm_1 = document.getElementById('pr-select-keyword-1');
    _registerPasswordDialog.selectRandomValue(keywordSelectElm_1);
    let keywordSelectElm_2 = document.getElementById('pr-select-keyword-2');
    _registerPasswordDialog.selectRandomValue(keywordSelectElm_2);
    let keywordSelectElm_3 = document.getElementById('pr-select-keyword-3');
    _registerPasswordDialog.selectRandomValue(keywordSelectElm_3);
    let keywordSelectElm_4 = document.getElementById('pr-select-keyword-4');
    _registerPasswordDialog.selectRandomValue(keywordSelectElm_4); 
    let keywordSelectElm_5 = document.getElementById('pr-select-keyword-5');
    _registerPasswordDialog.selectRandomValue(keywordSelectElm_5); 
}
function onRegisterDialog_ok() {
    console.log("onRegisterDialog_ok entry");
    _secretKeywordsArr = _kwdCtrl.createSecretKewrodsArr([
        document.getElementById('pr-select-keyword-1').value,
        document.getElementById('pr-select-keyword-2').value,
        document.getElementById('pr-select-keyword-3').value,
        document.getElementById('pr-select-keyword-4').value
    ], document.getElementById('pr-password-field').value);  
    let dialog = document.getElementById('dialog-2');
    dialog.close();
}