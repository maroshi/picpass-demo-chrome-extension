// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Global accessor that the popup uses.


// chromium URL
var _pluginUrl = `chrome-extension://${chrome.app.getDetails().id}/`;

// var _pluginUrl = ''
var _defaultScreenParams = {
    rows: 4,
    columns: 3,
    selectionsPerScreen: 1,
    maxSelections: 4, // max selections for password entries
    submitOnCompletedEntry: false, // automatically submit when entry is completed
    pluginUrl: _pluginUrl
};
var tabTitlesArr = {};
var selectedTabId = null;
var hasPasswordMessage = {
    type: "hasPasswordField",
    param: _defaultScreenParams
};

function updateTab(tabId) {
    chrome.storage.local.get(['param'], function(loadedObj) {
        hasPasswordMessage.param = _defaultScreenParams;
        if (loadedObj.param !== undefined) { hasPasswordMessage.param = loadedObj.param; }
        hasPasswordMessage.param.pluginUrl = _pluginUrl;

        chrome.tabs.sendMessage(tabId, hasPasswordMessage, function(hasPasswordFlag) {
            let title = (hasPasswordFlag) ? "Has password" : "No password found";
            tabTitlesArr[tabId] = title;
            if (!hasPasswordFlag) {
                chrome.pageAction.hide(tabId);
            } else {
                chrome.pageAction.show(tabId);
            }
            if (selectedTabId == tabId) {
                updateSelectedTabTitle(tabId);
            }
        });
    });

}

function updateSelectedTabTitle(tabId) {
    var tabTitle = tabTitlesArr[tabId];
    if (tabTitle)
        chrome.pageAction.setTitle({ tabId: tabId, title: tabTitle });
}

chrome.tabs.onUpdated.addListener(function(tabId, change, tab) {
    console.log(`${new Date()} chrome.tabs.onUpdated on ${tabId}`);
    if (change.status == "complete") {
        updateTab(tabId);
    }
});

chrome.tabs.onSelectionChanged.addListener(function(tabId, info) {
    console.log(`${new Date()} chrome.tabs.onSelectionChanged on ${tabId}`);
    selectedTabId = tabId;
    updateSelectedTabTitle(tabId);
});

// Ensure the current selected tab is set up.
chrome.tabs.query({ active: true, currentWindow: true }, function(tabs) {
    console.log(`${new Date()} chrome.tabs.query on ${tabs[0].id}`);
    updateTab(tabs[0].id);
});